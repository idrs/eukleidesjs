
Figure = _ block:Block ___?{return block}

Block = hd:Statement tl:(___ _ Statement)*  { return [hd].concat(tl.map(v=>v[2])); }

Statement = Draw
		/ Label
        / Command
        / Definition
        / Declaration
        / Assignment
        / Locus
        / ControlBlock
        / FunCall
		/ Local
		/ Return

Command = nc:("box"/"frame") __ args:ArgList {return [nc,args];}


Locus = "locus" __ n:Name _ "("_ t:Name _ "=" _ i:Interval _ ")" ___ b:LocusBlock ___ "end"
  {return ["locus",n,t,i,b];}

Interval = a:Expr __ "to" __ b:Expr __ "step" __ s:Expr {return ["interval",a,b,s];}
      / a:Expr __ "to" __ b:Expr {return ["interval",a,b,null];}



LocusBlock = hd:Statement ___ b:LocusBlock {return [hd].concat(b);}
            / "put" __ o:Object {return ["put",o];}

Draw = "draw" prp:PropList ___ bl:DrawBlock "end"
          {return bl.map(
                    function(v){
                      v[2]=v[2].concat(prp);
                      return v;
                    })
                  }
        / "draw" __ st:DrawStat {return st;}

ControlBlock = IfBlock / WhileBlock / ForBlock

IfBlock = "if" __ ifassr:Assertion ___ ifblk:Block ___ elblk:(ElseBlock)*  "end" {return ["if", ifassr,ifblk,elblk]}

ElseBlock =  elif:(ElifBlock)* "else" ___ blk:Block ___{return elif?["elif",elif,["else",blk]]:["else",blk]}

ElifBlock = "elseif" __ assr: Assertion ___ blk:Block ___{return ["elseif",assr,blk]}

WhileBlock = "while" __ assr: Assertion ___ blk:Block ___ "end" {return ["while",assr,blk]}

ForBlock = "for" __ range: Range ___ blk:Block ___ "end" {return ["for",range,blk]}

Range = a:Name __ "in" __ s:Name {return ["inrange",a,s]}
    / a:Name _ "=" _ d:Atom __ "to" __ f:Atom {return ["range",a,d,f,1]}  
    / a:Name _ "=" _ d:Atom __ "to" __ f:Atom __ "step" __ st:Atom {return ["range",a,d,f,st]}

Assertion = BoolOp / Comparison / SetAssert / GeoAssert / BoolCste

BoolCste = "true" {return ["bool","true"]} / "false" {return ["bool","false"]}

BoolOp = "not" __ a:Assertion {return ["not",a]}
	/ "(" _ a:Assertion __ "and" __ b: Assertion _ ")" {return ["and",a,b]}
	/ "(" _ a:Assertion __ "or" __ b: Assertion _ ")" {return ["or",a,b]}

Comparison = a:Expr _ "==" _ b:Expr {return ["eq",a,b]}  
	/ a:Expr _ "<" _ b:Expr {return ["<",a,b]}
    / a:Expr _ ">" _ b:Expr {return [">",a,b]}
    / a:Expr _ "!=" _ b:Expr {return ["neq",a,b]}  
	/ a:Expr _ "<=" _ b:Expr {return ["<=",a,b]}
    / a:Expr _ ">=" _ b:Expr {return [">=",a,b]}
    
SetAssert = a:Name __ "in" __ s:Name {return ["in",a,s]}
		/ "empty(" _ s:Name _")" {return ["empty?",s]}

GeoAssert = a:Name __ "on" __ s:Name {return ["on",a,s]}

DrawBlock = st:(_ DrawStat ___)+ {return st.map(v=>v[1]);}

DrawStat = 	n:DrawExp  prp:PropList {return ["draw",n,prp];}
          / n:DrawExp {return ["draw",n,[]]}

DrawExp = Object
		/ "(" n:Object ")" {return ["closed",n]}
        / "[" n:Object "]" {return ["filled",n]}
		/ "[" n:Object "]" a:Angle {return ["hash",n,a];}
        / str:String pt:Object a:Angle {return ["string",str,pt,a];}

Label = "label" prp:PropList ___ bl:LabelBlock "end"
          {return bl.map(
                    function(v){
                      v[2]=v[2].concat(prp);
                      return v;
                    })
                  }
        / "label" __ st:LabelStat {return st;}

LabelBlock = st:(_ LabelStat ___)+ {return st.map(v=>v[1]);}

LabelStat = 	n:LabelExp  prp:PropList {return ["label",n,prp];}
          / n:LabelExp {return ["label",n,[]]}

LabelExp =  AngleP
		/ Object

AngleP = a:Name _ "," _ b:Name _ "," _ c:Name {return ["angle",[a,b,c]];}

Object =
	 FunCall
    / Set
    / Name


Set = hd:Name tail:('.' Name)+ { return ["set",hd].concat(tail.map(v=>v[1]));}

Definition = Triangle / nm:NameList __ sh:Shape prp:PropList { return [sh,nm,prp] }

Triangle = p1:Name __ p2:Name __ p3:Name __ sh:TriangleShapeAndParam {return [sh,[p1,p2,p3]]}

TriangleShapeAndParam =  n:("triangle"/"right"/"iscosceles"/ "equilateral") _ p:Param?  {var msg=p?p.length:"vide";console.log(msg); return [n,p]}
  
Param = hd:Arg tl:("," _ Arg)|..3|{return [[hd],tl.flat().filter((it)=>it.length>0&&it!=",")].flat()}

Declaration = ex:Name __ nm:Name _ '(' _ args:TypedArgList _ ')' ___ body:Block ___ "end" {return ["declaration",[nm,ex,args,body]]}

Local = "local" __ hd:Name tl:(',' _ Name)* {return ["local",hd].concat(tl.map(v=>v[2])); } 

Return = "return" __ r:Expr {return ["return",r]}

NameList = hd:Name tl:(__ Name)+ { return [hd].concat(tl.map(v=>v[1])); }


Assignment = nm:(Set/Name) _ "=" _ exp:(Object / Expr) { return ["=",nm,exp];}

PropList = hd:(__ Prop)? tl:(_ ',' _ Prop)* {return (hd?[hd[1]].concat(tl.map(v=>v[3])):[]);}

Keyword = ControlKw !Name
    / PropKw !Name
    / Shape !Name

ControlKw = 'while'/ 'if'/ 'else'/ 'elseif'/'for'/'locus'/'put'/'box'/'frame'/'scale'/ 'draw'/ 'label'/ 'end'/'not'/'and'/'or'/'in'/'on'

Prop = Angle / Number / PropKw

PropKw = 'simple'/'double'/'triple'/'cross'/'forth'/'back'/'none'/'full'/'dotted'/ 'dashed'/'black'/'darkgray'/'gray'/'lightgray'/'white'/'red'/'green'/'blue'/'cyan'/'magenta'/'yellow'/ 'dot'/'disc'/'box'/'plus'/'cross'/'entire'/'half'/'arrow'/'arrows'/'right'

Shape = 'parallelogram'/ 'rectangle'/ 'square'/ 'triangle'/ 'isosceles'/ 'equilateral'/'right'

FunCall = nm:Name _ "(" _ args:ArgList _ ")" {return [nm,args];}

ArgList = hd:Arg tl:(_','_ Arg)* {return [hd].concat(tl.map(v=>v[3]));}

TypedArgList = tp:Name __ hd:Arg tl:(_','_ Name Arg)* {return [[hd,tp]].concat(tl.map(v=>[v[4],v[3]]));}


Arg =  Angle
	/ Expr
	/ Object
    / String

Name = !Keyword [a-zA-Z_][a-zA-Z0-9_']* {return text();}

Angle = e:Expr _ ("°"/"deg") {return ["deg",e];}
	/ e:Expr _ 'rad'  {return ["rad",e];}

Expr = hd:(Term)? _ op:('+'/'-') _ tl:Expr {return [op,hd,tl];}
	/ Term

Term = hd:Factor _ op:('*' / '/' / 'mod') _ tl:Term {return [op,hd,tl];}
	/ Factor

Factor = e:Atom _ "^" _ exp:Factor {return ["^",e,exp]}
		/Atom

Atom =  "(" _ e:Expr _ ")" {return e;}
		/ Number
        / Object
        / Pi

Number = '-'?[0-9]+('.'[0-9]+)*(('e'/'E')[0-9]+)* { return ["number",parseFloat(text())]; }

Pi = "pi" {return ["number",math.pi]}

String = '"' txt:[^"]* '"' {return ["str",text(txt)]}

_ "whitespace"
  = [ \t]*
__ = [ \t]+
___ = _ ';'/ (_ '\n')+
