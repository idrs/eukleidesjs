import parser from './js/eukl'

var program = parser.parse("A B C right 6, 35°"))

var world = {
    config: {
        xmin: -2,
        ymin: -2,
        xmax: 8,
        ymax: 6
    },
    draw: []
}

//création d'un point
function assign_point_cart(nom,x,y){
    world[nom] = 
    { nom:nom
    , type:'point'
    , x:x 
    , y:y
    }
}

//abscisse d'un point ou d'un vecteur
function abscissa(a){
    if(a.x){return a.x;}
    else{
        return a.r*Math.cos(a.th*Math.PI/180);
    }
}

//ordonnée d'un point ou d'un vecteur
function ordinate(a){
    if(a.y){return a.y;}
    else{
        return a.r*Math.sin(a.th*Math.PI/180);
    }
}

//longueur d'un vecteur
function length(u){
    if(u.type=="vector"){
        if(!u.r){
            u.r = Math.sqrt(Math.pow(abscissa(u),2)+Math.pow(ordinate(u),2));
        }
        return u.r;
    }
} 

//calcule la distance de A à B
function distance(A,B){
    return length(vector(A,B));
}

//renvoie le vecteur de A à B en cartésien
function vector(A,B){
    return {
        type:'vector',
        x: abscissa(B)-abscissa(A),
        y: ordinate(B)-ordinate(A)
    }
}

//evalue la valeur de l'expression 
function eval_expr(e){
    if(type(e)=='array'){
        switch(e[0]){
            case "+":
                return eval_expr(e[1])+eval_expr(e[2]);
                break;
            case "-":
                return eval_expr(e[1])-eval_expr(e[2]);
                break;
            case "*":
                return eval_expr(e[1])*eval_expr(e[2]);
                break;
            case "/":
                return eval_expr(e[1])/eval_expr(e[2]);
                break;
            case "^":
                return Math.pow(eval_expr(e[1]),eval_expr(e[2]));
                break;
            case "rad":
                return eval_expr(e[1])*Math.PI/180;
            default:
                console.log("Error: opération non-reconnue");
        }
    }
    else {
        return e;
    }
}

var apply_cases = {
    "declaration": function(args){},
    "=":function(args){},
    "for":function(args){},
    "triangle":function(args){
        var points = args[0];
        var props = args[1];
        var nombre_props = props.length();
        if(typeof(points)=="array"){
            var points_predefinis = points.map((pt)=>world[pt]).filter((pt)=>pt);
            var P1,P2,P3;            
            switch(points_predefinis.length()){
                case 0:
                    assign_point_cart(points[0],0,0);
                    P1 = world[points[0]];
                    
                case 1:

            }
        }

    }
    "right":function(args){
        var points = args[0];
        var props = args[1];
        if(typeof(points)=="array"){
            var points_predefinis = points.map((pt)=>world[pt]).filter((pt)=>pt);
            var P1,P2,P3;            
            switch(points_predefinis.length()){
                case 0:
                    assign_point_cart(points[0],0,0);
                    P1 = world[points[0]];
                    
                case 1:

                case 2:
                    //[P1,P2]=points_predefinis;
                    var p12 = vector(P1,P2);
                    var x = distance(P1,P2);
                    if(props[0]){
                        var type_expr = props[0];
                        if(type_expr == 'number'){
                            var y = eval_expr(props[1]); 
                        }
                        if(type_expr == "rad" | type_expr == "deg"){
                            var u = eval_expr(props[1]);
                            var y = x*Math.tan(u);
                        }
                    }
                    else{
                        y = x*3/4;
                    }
                    var P3 = points[2];
                    var xp12u = abscissa(p12)/x;
                    var yp12u = ordinate(p12)/x;
                     
                    var x3 = abscissa(P2)-y*yp12u;
                    var y3 = ordinate(P2)+y*xp12u;
                    assign_point_cart(P3,x3,y3);
                    break;
                case 1:    
                    [P1]=points_predefinis;
                    //var p12 = vector(P1,P2);
                    //var x = distance(P1,P2);
                    if(props[0]){
                        var type_expr = props[0];
                        if(type_expr == 'number'){
                            var y = eval_expr(props[1]); 
                        }
                        if(type_expr == "rad" | type_expr == "deg"){
                            var u = eval_expr(props[1]);
                            var y = x*Math.tan(u);
                        }
                    }
                    else{
                        y = x*3/4;
                    }
                    var P3 = points[2];
                    var xp12u = abscissa(p12)/x;
                    var yp12u = ordinate(p12)/x;
                    
                    var x3 = abscissa(P2)-y*yp12u;
                    var y3 = ordinate(P2)+y*xp12u;
                    assign_point_cart(P3,x3,y3);
                    break;
            }
        }
    },

}

function apply(statement){
    var name, args;
    [name, ...args] = statement;
    try{
        apply_cases[name](args);
    } 
    catch(error){
        console.log("Error in apply statement: ");
        console.log(error);
    }
}

function build(program){
    program.foreach(apply)
}