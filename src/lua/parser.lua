local lpeg = require("lpeg")

local P,C,S,V = lpeg.P, lpeg.C, lpeg.S, lpeg.V;

local white = lpeg.S(" \t") ^ 0
local wspace = S(" \t")^1
local eol = lpeg.S(";\n")^1
local integer = white * lpeg.R("09") ^ 1 / tonumber
local variable = white * lpeg.R("az","AZ")^1 

local eukgrammar = lpeg.P {
    "prog",
    prog = (V"statement")^1,
    statement = white * (V"definition" + V"declaration" + V"draw" + V"label") * (eol + -1),
    definition =  ,
    declaration = ,
    draw = ,
    label = ,   
}